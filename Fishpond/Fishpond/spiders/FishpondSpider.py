from scrapy import log # This module is useful for printing out debug information
from scrapy.spider import Spider
from scrapy import Selector
from scrapy.http import Request

class FishpondSpider(Spider):
	name = "fishpond"
	allowed_domains = ["fishpond.com.au"]
	start_urls = [""]
	
	# This gets the arguments that are posted to the spider
	def __init__(self, barcode='', *args, **kwargs):
		super(FishpondSpider, self).__init__(*args, **kwargs)
		
		# Construct the url
		url = "http://www.fishpond.com.au/q/"
		url += barcode
		self.start_urls = [url] 
		
	def parse(self,response):
		
		# Setup the selector
		sel = Selector(response)
		
		# Get the title
		title = sel.xpath('//*[@id="product_title"]/span[1]/text()').extract()
		print "The title is below"
		print str(title)

