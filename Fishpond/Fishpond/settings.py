# -*- coding: utf-8 -*-

# Scrapy settings for Fishpond project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'Fishpond'

SPIDER_MODULES = ['Fishpond.spiders']
NEWSPIDER_MODULE = 'Fishpond.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'Fishpond (+http://www.yourdomain.com)'
